from tkinter import *
from App import *

root = Tk()
app = App(root)
root.protocol('WM_DELETE_WINDOW', app.quit) 
root.mainloop()
