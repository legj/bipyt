from tkinter import filedialog
from tkinter import messagebox
from tkinter import *
from Canvas import *
from ImgManip import *
import numpy

def donothing():
	print("a")

class App:
	"""Creates main window, modal, menus"""

	def __init__(self, win):
		self.__win = win
		size = self.__width, self.__height = 640, 480
		self.canvas = Canvas(self, win, size)
		self.imgmanip = ImgManip(self, self.canvas)
		self.__operationsStatus = {"image": True, "save": True}
		self.createWindow()

	def convolutionSettingsCallback(self, modal, entries):
		"""Reads convolution matrix and sends it to ImgManip object"""
		mask = numpy.zeros((3, 3))
		i = 0
		for entry in entries:
			val = 0
			try:
				val = entry.get()
				if val == "":
					val = 0
				else:
					val = int(val)
			except:
				messagebox.showerror("Error", "Matrix should contain only integers.")
				return
			mask[i // 3][i % 3] = val
			i += 1

		self.imgmanip.manipulate("convolution", mask.astype(int))

		modal.destroy()


	def showConvolutionSettings(self):
		"""Opens dialog so user can input convolution matrix"""
		if not self.isOperationEnabled("image"):
			return
		modal = Toplevel(self.__win)
		modal.title("Convolution matrix")
		modal.geometry("{}x{}".format(200, 120))
		modal.resizable(width = False, height = False)
		entries = [Entry(modal, width=3) for i in range(9)]
		i = 0
		for entry in entries:
			entry.grid(row = i // 3, column = i % 3)
			i += 1

		frame = Frame(modal)
		frame.grid(columnspan = 3, sticky = S + E + W, pady = 10)
		buttonOk = Button(frame, text = 'Ok', width = 8, command = lambda: self.convolutionSettingsCallback(modal, entries))
		buttonOk.pack(side = LEFT)
		buttonCancel = Button(frame, text = 'Cancel', width = 8, command = lambda: modal.destroy())
		buttonCancel.pack(side = RIGHT)

	def createWindow(self):
		"""Creates main window"""
		win = self.__win
		win.title("Light Image Editor")
		win.geometry("{}x{}".format(self.__width, self.__height))
		win.resizable(width = False, height = False)
		self.createMenu(win)

	def isOperationEnabled(self, op):
		"""Checks if given set of operations is enabled"""
		return self.__operationsStatus[op]

	def disableImageOperations(self):
		"""Disables a menu item and sets image operation as disabled"""
		self.__operationsStatus["image"] = False
		self.__menu.entryconfig("Image", state = "disabled")
	def enableImageOperations(self):
		"""Enables a menu item and sets image operation as enabled"""
		self.__operationsStatus["image"] = True
		self.__menu.entryconfig("Image", state = "normal")

	def disableSaveOperations(self):
		"""Disables a menu item and sets save operation as disabled"""
		self.__operationsStatus["save"] = False
		self.__fileMenu.entryconfig("Save as", state = "disabled")
	def enableSaveOperations(self):
		"""Enables a menu item and sets save operation as enabled"""
		self.__operationsStatus["save"] = True
		self.__fileMenu.entryconfig("Save as", state = "normal")

	def unsavedChangesDialog(self):
		"""This function asks user if he wants to continue even if he has unsaved changes."""
		if self.canvas.getChanged() == True and "yes" == messagebox.askquestion("Delete", "You have unsaved changes. Do you wish to continue?", icon = "warning"):
			return True
		return not self.canvas.getChanged()

	def quit(self):
		"""Program exit wrapper"""
		if not self.unsavedChangesDialog():
			return
		self.__win.quit()

	def createMenu(self, win):
		"""Creates main window's menu"""
		win = self.__win
		
		menu = Menu(win)

		fileMenu = Menu(menu, tearoff = 0)
		fileMenu.add_command(label = "Open", command = self.canvas.open, accelerator = "Ctrl+O")
		win.bind_all("<Control-o>", lambda event: self.canvas.open())
		fileMenu.add_command(label = "Save as", command = self.canvas.save, accelerator = "Ctrl+S")
		win.bind_all("<Control-s>", lambda event: self.canvas.save())
		fileMenu.add_separator()
		fileMenu.add_command(label = "Exit", command = self.quit, accelerator = "Ctrl+X")
		win.bind_all("<Control-x>", lambda event: self.quit())
		menu.add_cascade(label = "File", menu = fileMenu)

		imageMenu = Menu(menu, tearoff = 0)

		convolutionMenu = Menu(imageMenu, tearoff = 0)
		convolutionMenu.add_command(label = "Custom convolution mask", command = self.showConvolutionSettings, accelerator = "Ctrl+H")
		win.bind_all("<Control-h>", lambda event: self.showConvolutionSettings())
		convolutionMenu.add_command(label = "Sharpen", command = lambda: self.imgmanip.manipulate("convolution", numpy.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])), accelerator = "Ctrl+J")
		win.bind_all("<Control-j>", lambda event: self.imgmanip.manipulate("convolution", numpy.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])))
		convolutionMenu.add_command(label = "Blur", command = lambda: self.imgmanip.manipulate("convolution", numpy.array([[1, 1, 1], [1, -7, 1], [1, 1, 1]])), accelerator = "Ctrl+K")
		win.bind_all("<Control-k>", lambda event: self.imgmanip.manipulate("convolution", numpy.array([[1, 1, 1], [1, -7, 1], [1, 1, 1]])))
		imageMenu.add_cascade(label = "Convolution", menu = convolutionMenu)

		imageMenu.add_command(label = "A lot of shades of grey", command = lambda: self.imgmanip.manipulate("greyscale"), accelerator = "Ctrl+G")
		win.bind_all("<Control-g>", lambda event: self.imgmanip.manipulate("greyscale"))
		imageMenu.add_command(label = "Invert colors", command = lambda: self.imgmanip.manipulate("invert"), accelerator = "Ctrl+I")
		win.bind_all("<Control-i>", lambda event: self.imgmanip.manipulate("invert"))
		imageMenu.add_command(label = "Lighten", command = lambda: self.imgmanip.manipulate("lightness", 10), accelerator = "Ctrl+L")
		win.bind_all("<Control-l>", lambda event: self.imgmanip.manipulate("lightness", 10))
		imageMenu.add_command(label = "Darken", command = lambda: self.imgmanip.manipulate("lightness", -10), accelerator = "Ctrl+D")
		win.bind_all("<Control-d>", lambda event: self.imgmanip.manipulate("lightness", -10))


		menu.add_cascade(label = "Image", menu = imageMenu)

		win.config(menu = menu)

		self.__menu = menu
		self.__fileMenu = fileMenu

		self.disableSaveOperations()
		self.disableImageOperations()
