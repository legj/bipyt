from PIL import Image, ImageTk
import numpy

class ImgManip:
	"""This class does all the work concerning image editation"""

	def __init__(self, app, canvas):
		self.app = app
		self.canvas = canvas

	def manipulate(self, action, settings = None):
		"""This function decides which manipulation worker function to run"""
		if not self.app.isOperationEnabled("image"):
			return
			
		func = None
		if action == "greyscale":
			func = self.greyscale
		elif action == "invert":
			func = self.invert
		elif action == "lightness":
			func = self.lightness
		elif action == "convolution":
			func = self.convolution

		if func == None:
			raise Exception("Variable 'func' is set to an unsupported value.")

		origImg = self.canvas.getImg()
		img = func(origImg, settings)
		self.canvas.setImg(img)

		origThumb = self.canvas.getThumb()
		thumb = func(origThumb, settings)
		self.canvas.setThumb(thumb)

		self.canvas.show()

	def greyscale(self, im, settings):
		"""Converts PIL Imge to greyscale"""
		width, height = im.size

		pix = numpy.array(im)
		grey = numpy.empty_like(pix)

		r, g, b = pix[:,:,0], pix[:,:,1], pix[:,:,2]
		lvl = 0.2126 * r + 0.7152 * g + 0.0722 * b
		for c in range(3):
			grey[:,:,c] = lvl

		return Image.fromarray(grey)

	def lightness(self, im, k):
		"""Lightens or darkens image"""
		width, height = im.size

		pix = numpy.array(im).astype(int)
		res = numpy.empty_like(pix)

		r, g, b = pix[:,:,0], pix[:,:,1], pix[:,:,2]
		res[:,:,0] = numpy.clip(k + r, 0, 255)
		res[:,:,1] = numpy.clip(k + g, 0, 255)
		res[:,:,2] = numpy.clip(k + b, 0, 255)

		return Image.fromarray(res.astype('uint8'))

	def invert(self, im, settings):
		"""Invert colors int the image"""
		width, height = im.size

		pix = numpy.array(im)

		r, g, b = pix[:,:,0], pix[:,:,1], pix[:,:,2]
		pix[:,:,0] = 255 - r
		pix[:,:,1] = 255 - g
		pix[:,:,2] = 255 - b

		return Image.fromarray(pix)

	def convolution(self, im, mask):
		"""Apply a convolution matrix"""

		width, height = im.size
		pix = numpy.array(im).astype(int)
		conv = numpy.empty_like(pix)

		conv = pix[:,:] * mask[1][1]

		mutation = numpy.roll(pix, 1, axis = 1)
		conv += mutation * mask[1][0]
		mutation = numpy.roll(pix, -1, axis = 1)
		conv += mutation * mask[1][2]
		mutation = numpy.roll(pix, -1, axis = 0)
		conv += mutation * mask[2][1]
		mutation = numpy.roll(pix, 1, axis = 0)
		conv += mutation * mask[0][1]

		mutation = numpy.roll(numpy.roll(pix, 1, axis = 1), 1, axis = 0)
		conv += mutation * mask[0][0]
		mutation = numpy.roll(numpy.roll(pix, -1, axis = 1), 1, axis = 0)
		conv += mutation * mask[0][2]
		mutation = numpy.roll(numpy.roll(pix, -1, axis = 1), -1, axis = 0)
		conv += mutation * mask[2][2]
		mutation = numpy.roll(numpy.roll(pix, 1, axis = 1), -1, axis = 0)
		conv += mutation * mask[2][0]

		conv[0,:,:] = pix[0,:,:]
		conv[:,0,:] = pix[:,0,:]
		conv[height - 1,:,:] = pix[height - 1,:,:]
		conv[:,width - 1,:] = pix[:,width - 1,:]

		conv = numpy.clip(conv[:,:,:], 0, 255)

		return Image.fromarray(conv.astype('uint8'))