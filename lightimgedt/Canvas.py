from tkinter import messagebox
from tkinter import filedialog
from tkinter import Canvas as CanvasElement
from tkinter import *
from PIL import Image, ImageTk
import math

class Canvas:
	"""This class takes care of loading/saving image and changing it in the window"""


	def __init__(self, app, win, size):
		self.setUnchanged()
		self.__im = 0
		self.__thumb = 0
		self.app = app
		self.__size = size
		self.__win = win
		self.__size = self.__width, self.__height = 640, 480
		self.createCanvas(win)

	def createCanvas(self, win):
		"""Creates canvas element on the main window"""
		canvas = CanvasElement(win, width = self.__width, height = self.__height)
		canvas.pack()
		self.canvas = canvas

	def show(self):
		"""Sets content of canvas element to edited/open image"""
		if self.__thumb != 0:
			self.setChanged()
			self.__thumbConv = ImageTk.PhotoImage(self.__thumb)
			self.canvas.create_image(self.__width // 2, self.__height // 2, image = self.__thumbConv)

	def getThumb(self):
		"""getter for thumb"""
		return self.__thumb
	def setThumb(self, thumb):
		"""setter for thumb"""
		self.__thumb = thumb

	def getImg(self):
		"""getter for image"""
		return self.__im
	def setImg(self, img):
		"""setter for image"""
		self.__im = img

	def save(self):
		"""This method saves the image"""
		if not self.app.isOperationEnabled("save"):
			return
		filename = filedialog.asksaveasfilename(confirmoverwrite = True, filetypes = [("Image", "*.png")])
		if not filename:
			return
		try:
			self.__im.save(filename)
		except Exception as e:
			messagebox.showerror("Error", str(e))
			return
		self.setUnchanged()

	def open(self):
		"""This method opens a new file"""
		if not self.app.unsavedChangesDialog():
			return
		filePath = filedialog.askopenfilename()
		if filePath.__len__() == 0:
			return
		try:
			self.__im = im = Image.open(filePath)
			pilim = ImageTk.PhotoImage(im)
			idealShowSize = self.getIdealSize((pilim.width(), pilim.height()), self.__size)
			self.__thumb = im.resize(idealShowSize)
		except Exception as e:
			messagebox.showerror("Error", str(e))
			return
		self.show()
		self.setUnchanged()
		self.app.enableImageOperations()
		self.app.enableSaveOperations()

	def getIdealSize(self, s, smax):
		"""This method determines the size of image preview"""
		if s < smax:
			return s

		w, h = s
		mw, mh = smax
		kw = w / mw
		kh = h / mh
		k = max(kw, kh)

		return math.floor(w / k), math.floor(h / k)

	def setChanged(self):
		"""Sets canvas state as changed"""
		self.__changed = True
	def setUnchanged(self):
		"""Sets canvas state as unchanged"""
		self.__changed = False
	def getChanged(self):
		"""Getter for changed state"""
		return self.__changed